module Types where
import Data.Time.Clock.System
import Data.Vector
import Foreign.C.Types
import Graphics.UI.FLTK.LowLevel.FLTKHS
import Graphics.UI.Gezira.Gezira
import Foreign.Ptr

data Flake =
  Flake
  {
    flakeX :: CFloat
  , flakeY :: Ptr CFloat
  , flakeDy :: CFloat
  , flakeScale :: CFloat
  , flakeAngle :: Ptr CFloat
  , flakeDangle :: CFloat
  } deriving Show

data UIConstants =
  UIConstants
  {
    snowflakeBox :: Ref Box
  , image :: GeziraImage
  }

data UIState =
  UIState
  {
    nthreads :: CInt
  , flakes :: Vector Flake
  , before :: Maybe SystemTime
  , nileProcess :: NileProcess
  , zoom :: CFloat
  , changed :: Bool
  }

data Matrix =
  Matrix
  {
    matrixA :: CFloat
  , matrixB :: CFloat
  , matrixC :: CFloat
  , matrixD :: CFloat
  , matrixE :: CFloat
  , matrixF :: CFloat
  } deriving Show
