{-# LANGUAGE ScopedTypeVariables, OverloadedStrings #-}
module Main where
import Data.IORef
import Data.Vector
import Foreign
import Foreign.Ptr
import Foreign.C.Types
import Foreign.Marshal.Alloc
import Graphics.UI.FLTK.LowLevel.FLTKHS
import Graphics.UI.FLTK.LowLevel.Fl_Enumerations
import Graphics.UI.FLTK.LowLevel.Fl_Types
import Graphics.UI.Gezira.Gezira
import Graphics.UI.Gezira.Nile
import qualified Control.Monad as M
import SnowDemo
import System.Random
import Types
import Constants
import qualified Graphics.UI.FLTK.LowLevel.FL as FL
import Control.Concurrent
import Data.Time.Clock.System
import qualified Data.ByteString as B
import qualified Data.ByteString.Unsafe as B
import System.Mem
import qualified System.IO.Unsafe as Unsafe
import Debug.Trace

matrixTranslate :: CFloat -> CFloat -> Matrix -> Matrix
matrixTranslate x y matrix =
  matrix
  {
    matrixE = (matrixA matrix) * x + (matrixC matrix) * y + (matrixE matrix)
  , matrixF = (matrixB matrix) * x + (matrixD matrix) * y + (matrixF matrix)
  }

matrixRotate :: CFloat -> Matrix -> Matrix
matrixRotate t matrix =
  let sint = sin t
      cost = cos t
  in
  matrix
  {
    matrixA = (matrixA matrix) * cost + (matrixC matrix) * sint
  , matrixB = (matrixB matrix) * cost + (matrixD matrix) * sint
  , matrixC = (matrixA matrix) * (-sint) + (matrixC matrix) * cost
  , matrixD = (matrixB matrix) * (-sint) + (matrixD matrix) * cost
  }

matrixScale :: CFloat -> CFloat -> Matrix -> Matrix
matrixScale sx sy matrix =
  matrix
  {
    matrixA = (matrixA matrix) * sx
  , matrixB = (matrixB matrix) * sx
  , matrixC = (matrixC matrix) * sy
  , matrixD = (matrixD matrix) * sy
  }

snowflakesUpdate :: Vector Flake -> Int -> IO ()
snowflakesUpdate flakes height =
  let newFlake flake = do
        y <- peek (flakeY flake)
        angle <- peek (flakeAngle flake)
        poke (flakeY flake)
          (
            if (y > fromIntegral height + 10)
            then (-10)
            else y + (flakeDy flake)
          )
        poke (flakeAngle flake) (angle + (flakeDangle flake))
  in Data.Vector.mapM_ newFlake flakes

snowflakeOffScreen :: CFloat -> CFloat -> Int -> Int -> CFloat -> Bool
snowflakeOffScreen x y width height zoom =
  let dx = (fromIntegral width) / abs (x - (fromIntegral width) / 2)
      dy = (fromIntegral height) / abs (y - (fromIntegral height) / 2)
  in zoom > dx || zoom > dy

snowflakeRender :: Flake -> [CFloat] -> CFloat -> Int -> Int -> GeziraImage -> NileProcess -> IO ()
snowflakeRender flake path zoom width height image nileProcess =
  let matrixTransform y angle =
          matrixScale (flakeScale flake) (flakeScale flake)
        . matrixRotate angle
        . matrixTranslate (flakeX flake) y
        . matrixTranslate (fromIntegral (-width) / 2) (fromIntegral (-height) / 2)
        . matrixScale zoom zoom
        . matrixTranslate (fromIntegral width / 2) (fromIntegral height / 2)
   in do
     y <- peek (flakeY flake)
     angle <- peek (flakeAngle flake)
     let matrix = matrixTransform y angle matrixInit
     transformBeziers <-
          gezira_TransformBeziers
            nileProcess
            (matrixA matrix)
            (matrixB matrix)
            (matrixC matrix)
            (matrixD matrix)
            (matrixE matrix)
            (matrixF matrix)
     clipBeziers <-
       gezira_ClipBeziers nileProcess 0 0 (fromIntegral width) (fromIntegral height)
     rasterize <-
       gezira_Rasterize nileProcess
     composite <-
       gezira_CompositeUniformColorOverImage_ARGB32
         nileProcess
         image
         flakeAlpha
         flakeRed
         flakeGreen
         flakeBlue
     pipeline <-  nile_Process_pipe_v [transformBeziers, clipBeziers, rasterize, composite]
     withArrayLen
       path
       (
         \len ptr ->
           nile_Process_feed pipeline ptr (fromIntegral len)
       )

makeFlakes :: Int -> Int -> Int -> IO (Vector Flake)
makeFlakes boxWidth boxHeight nFlakes =
  generateM
    nFlakes
    (
      \_ ->
         Flake
           <$> randomRIO (0.0, (fromIntegral boxWidth))
           <*> do
               {
                 h <- randomRIO (0.0, (fromIntegral boxHeight));
                 p <- malloc;
                 poke p h;
                 return p;
               }
           <*> randomRIO (0.5, 3.0)
           <*> randomRIO (0.2, 0.7)
           <*> do
               {
                 angle <- randomRIO (0.0, 4.0);
                 p <- malloc;
                 poke p angle;
                 return p;
               }
           <*> randomRIO ((-0.1), 0.1)
    )

nanosPerFrame = truncate (1000000000 / 60)

updateFps :: UIConstants -> IORef UIState -> IO ()
updateFps uiConstants uiStateRef = do
  uiState <- readIORef uiStateRef
  let before' = before uiState
  now <- getSystemTime
  modifyIORef uiStateRef (\uiState -> uiState { before = Just now })
  case before' of
    Nothing -> return ()
    Just before ->
      let interval =
            (
              systemSeconds now - systemSeconds before
            , systemNanoseconds now - systemNanoseconds before
            )
          sync = nile_sync (nileProcess uiState)
      in
      case interval of
        (secs, _) | secs > 0 ->  sync
        (_,nanos) | nanos < nanosPerFrame -> sync
        _ -> return ()

updateBox :: GeziraImage -> Ref Box -> IO ()
updateBox image box = do
  bytes <- geziraImagePixels image
  (_,_,width,height) <- fromRectangle<$>getRectangle box
  newImage <- rgbImageNew bytes (Size (Width width) (Height height)) (Just (Depth 4)) Nothing
  FL.lock
  oldImage <- getImage box
  setImage box (Just newImage)
  FL.unlock
  maybe (return ()) destroy oldImage
  redraw box
  FL.awake

resetNileProcess :: UIConstants -> IORef UIState -> IO ()
resetNileProcess uiConstants uiStateRef = do
  uiState <- readIORef uiStateRef
  _ <- nile_sync (nileProcess uiState)
  processMemory <- nile_shutdown (nileProcess uiState)
  free processMemory
  let numBytes = (nthreads uiState) * nBytesPerThread
  newMemory <- mallocBytes (fromIntegral numBytes)
  newNileProcess <- nile_startup newMemory (fromIntegral numBytes) (nthreads uiState)
  gezira_Image_reset_gate (image uiConstants)
  modifyIORef uiStateRef
    (\uiState ->
       uiState
         {
           nileProcess = newNileProcess
         , Types.changed = False
         }
    )

snowflakeUpdate :: UIConstants -> IORef UIState -> IO ()
snowflakeUpdate uiConstants uiStateRef = do
  uiState <- do
    uiState' <- readIORef uiStateRef
    M.when (Types.changed uiState')
      (resetNileProcess uiConstants uiStateRef)
    readIORef uiStateRef
  width <- getW (snowflakeBox uiConstants)
  height <- getH (snowflakeBox uiConstants)
  callback <- toLoguePrim (updateBox (image uiConstants) (snowflakeBox uiConstants))
  p <- do
    newP <- nile_Process (nileProcess uiState) 1 (Just callback)
    if (not (nileProcessNull newP))
     then do
       nextGate <- nile_Process (nileProcess uiState) 1 Nothing
       nile_Process_gate newP nextGate
       oldGate <- geziraImageGate (image uiConstants)
       case oldGate of
         Just gate -> do
           p <- nile_Process_pipe_v [gate, newP]
           geziraImageSetGate (image uiConstants) nextGate
           return newP
         Nothing -> return newP
     else return newP
  spans <- gezira_RectangleSpans
             (nileProcess uiState)
             0 0
             (fromIntegral width)
             (fromIntegral height)
  composite <- gezira_CompositeUniformColorOverImage_ARGB32
                 (nileProcess uiState)
                 (image uiConstants)
                 1 0 0 0
  pipeline <- nile_Process_pipe_v [p, spans, composite]
  nile_Process_feed pipeline nullPtr 0
  Data.Vector.mapM_
    (
      \flake -> do
        y <- peek (flakeY flake)
        M.when (not (snowflakeOffScreen (flakeX flake) y width height (zoom uiState)))
          (snowflakeRender
             flake
             snowflakePath
             (zoom uiState)
             (fromIntegral width)
             (fromIntegral height)
             (image uiConstants)
             (nileProcess uiState))
    )
    (flakes uiState)
  snowflakesUpdate (flakes uiState) height
  updateFps uiConstants uiStateRef

initialNileProcess :: CInt -> IO NileProcess
initialNileProcess nthreads = do
  let numBytes = nthreads * nBytesPerThread
  bytes <- mallocBytes (fromIntegral numBytes)
  nile_startup bytes (fromIntegral numBytes) (fromIntegral nthreads)

spinnerCallback :: UIConstants -> IORef UIState -> Ref Spinner -> IO ()
spinnerCallback uiConstants uiStateRef threadSpinner = do
  numThreads <- getValue threadSpinner
  modifyIORef uiStateRef (\uiState -> uiState { nthreads = truncate numThreads , Types.changed = True })

zoomCallback :: IORef UIState -> Ref Slider -> IO ()
zoomCallback uiStateRef slider = do
  zoom <- getValue slider
  modifyIORef uiStateRef (\uiState -> uiState { zoom = realToFrac zoom , Types.changed = True })

makeImage :: Int -> Int -> Int -> IO GeziraImage
makeImage width height stride=
  gezira_Image_init (fromIntegral width) (fromIntegral height) (fromIntegral stride)

ui :: IO()
ui = do
  window <- doubleWindowNew  (Size (Width 649) (Height 662)) (Just (Position (X 305) (Y 170))) (Just "Let It Snow!")
  setColor window (Color 34)
  setLabelcolor window (Color 34)
  spinner <- spinnerNew  (toRectangle (90,11,40,24)) (Just "Threads")
  setMinimum spinner 1
  setMaximum spinner 10
  setValue spinner 4
  setLabelcolor spinner (Color 7)
  zoomSlider <- sliderNew  (toRectangle (220,11,155,24)) (Just "Zoom:")
  setMinimum zoomSlider 1
  setMaximum zoomSlider 8
  setAlign zoomSlider (Alignments [AlignTypeCenter,AlignTypeLeft])
  setType zoomSlider HorSliderType
  setLabelcolor zoomSlider (Color 7)
  setColor zoomSlider (Color 37)
  let (x,y,boxWidth, boxHeight) = (23,45,600,600)
  boxGroup <- groupNew (toRectangle (x,y,boxWidth, boxHeight)) Nothing
  begin boxGroup
  snowflakeBox <- boxNew (toRectangle (x,y,boxWidth,boxHeight)) Nothing
  setBox snowflakeBox FlatBox
  end boxGroup
  flakes <- makeFlakes boxWidth boxHeight nFlakes
  showWidget window
  FL.lock
  M.void
    (forkIO
      (do
        let nthreads = 4
        nileProcess <- initialNileProcess nthreads
        image <- makeImage boxWidth boxHeight boxWidth
        let uiConstants =
             UIConstants
             {
               snowflakeBox = snowflakeBox
             , image = image
             }
        uiStateRef <-
          newIORef
            UIState
              {
                nthreads = nthreads
              , flakes = flakes
              , before = Nothing
              , nileProcess = nileProcess
              , zoom = 1.0
              , Types.changed = False
              }
        setCallback spinner (spinnerCallback uiConstants uiStateRef)
        setCallback zoomSlider (zoomCallback uiStateRef)
        M.forever (snowflakeUpdate uiConstants uiStateRef)
      )
    )


main :: IO ()
main = ui >> FL.run >> FL.flush

replMain :: IO ()
replMain = ui >> FL.replRun
